# unity_test
## 概要
Unityのプロジェクトをgitで管理するテストです。

## Unityのプロジェクトを作成した際に最初にやること(暫定)
1. [Edit]>[Project Settings]>[Editor]
2. [Version Control]を[Meta Files]に変更
3. [Asset Serialization]を[Force Text]に変更
4. .gitignoreを追加

metaファイルのせいでコンフリクト起こったりするトラブルが発生する可能性あり(未確認)

### .gitignoreに書くこと
* Library
* Temp
* *.pidb
* *.unityproj
* *.sln
* *.userprefs
* *.csproj
* *~

AssetsとProjectSettings以外は無くても大丈夫らしいです。